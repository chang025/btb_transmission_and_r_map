# bTB_transmission_and_R_map

## information
This repository contains the code for the research paper on inferring bTB (bovine tuberculosis) transmission between cattle and badgers and generating an R map.

Please note that the infection data of cattle and badgers, as well as the spatial locations of herds and badger territories, are required for this analysis. However, due to data protection regulations, we cannot share the actual herd ID and locations in this repository. We have provided an example data frame in this repository without the actual herd ID and location information. This example data frame can be used for understanding the parameter estimation process.

To use this code, you will need to provide your own dataset with the herd ID and spatial locations when constructing the exposure for cattle and badgers. However, once the exposures are constructed, the herd ID and locations are no longer necessary for parameter estimation.


## how to use it

Prepare your own infection data for cattle and badgers, as well as the spatial locations of herds and badger territories (not provided in this repository) and calcualte the overlappting area among badger territories and farms. 

### Code 01, 02: Constructing Exposures 
Use the provided functions in Code 01 to construct the exposure for cattle and badgers based on your own data. Ensure that you replace the example data frame in the code with your dataset, including the herd ID and spatial locations.

### Code 03: Parameter Estimation
Once the exposures are constructed, run Code 02 to perform parameter estimation. This code utilizes maximum likelihood estimation to estimate the best-fit decay rate and transmission rate parameters for bTB transmission between cattle and badgers.

### Code 04: Sensitivity Analysis
Code 04 conducts a sensitivity analysis on different model structures. This analysis helps evaluate the impact of varying model parameters on the bTB transmission inference.

### Code 05: Generalized Additive Model (GAM)
The badger prevelance data is not precise as spatial scale in this model, therefore we use a generalized additive model (GAM) to predict badger prevalence on a temporal and spatial scale used in this study. The predicted badger prevelance is used in consturcting exposure. 

### Code 06: Next Generation Matrix (NGM) Method
Code 06 demonstrates the Next Generation Matrix (NGM) method to calculate the within-herd R, a measure of transmission potential within a herd. This code also includes a sensitivity analysis when the decay rate parameter is set to a higher value of 0.02 per day.

### Code 07: Local Transmission Model
Code 07 shows how to build a local transmission model among a herd and its adjacent badger territories, as well as directly connected herds, using the SimInf package. This code simulates local transmission in each herd with 200 repeats. The simulation is performed on a high-performance cluster using a total of 1335 spatial configurations for the study area.

### Code 08: Between Herd R Calculation and Mapping
Code 08 demonstrates how to calculate the between-herd R using the simulation summary. Additionally, it provides instructions on how to generate a map of the between-herd R values, visualizing the bTB transmission between cattle herds.




