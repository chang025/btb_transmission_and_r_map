# we assume cattle number (Nc) as a proxy of the area
# however, we also did sensitivity analysis on different assumption 
# such as using Nb, or Nb + NC or area as the proxy of the area.

GLM_cattle_data %>% 
  filter(ExpB == 0 & Exp == 0 & case > 0)  # 25 cattle cannot be expained 
GLM_cattle_data %>% 
  filter(ExpB != 0|Exp != 0|case <= 0)%>%
  filter(case < total_animals) -> GLM_cattle_data

left_join(GLM_cattle_data, herd_nc_nb, by = "herd_no")-> GLM_cattle_data

left_join(GLM_cattle_data, AHCS_KK_area) -> GLM_cattle_data 

GLM_cattle_data %>%
  mutate(Nc = 
           ifelse((total_animals < 30), 30, total_animals)) -> GLM_cattle_data



loglik_c_Nc <- function(beta){
  betaCC <- beta[1]
  betaBC <- beta[2]
  
  AIC <-
    -2*sum(dbinom(GLM_cattle_data$case, GLM_cattle_data$S,
                  prob = (1-exp(-(betaCC*GLM_cattle_data$Exp/GLM_cattle_data$Nc +
                                    betaBC*GLM_cattle_data$ExpB/GLM_cattle_data$Nc))), 
                  log=TRUE))+4 
  return(AIC)
}

betaCC = 1e-5
betaBC = 1e-5
loglik_c_Nc(c(1e-5, 1e-5))

ml_c_Nc <- optim(c(1e-5, 1e-6), loglik_c_Nc)
ml_c_Nc$value

loglik_c_Nb <- function(beta){
  betaCC <- beta[1]
  betaBC <- beta[2]
  
  AIC <-
    -2*sum(dbinom(GLM_cattle_data$case, GLM_cattle_data$S,
                  prob = (1-exp(-(betaCC*GLM_cattle_data$Exp/GLM_cattle_data$Nb +
                                    betaBC*GLM_cattle_data$ExpB/GLM_cattle_data$Nb))), 
                  log=TRUE))+4 
  return(AIC)
}

ml_c_Nb <- optim(c(1e-8, 1e-8), loglik_c_Nb)
ml_c_Nb

loglik_c_NbNc <- function(beta){
  betaCC <- beta[1]
  betaBC <- beta[2]
  
  AIC <-
    -2*sum(dbinom(GLM_cattle_data$case, GLM_cattle_data$S,
                  prob = (1-exp(-(betaCC*GLM_cattle_data$Exp/(GLM_cattle_data$Nb+GLM_cattle_data$Nc) +
                                    betaBC*GLM_cattle_data$ExpB/(GLM_cattle_data$Nb+ GLM_cattle_data$Nc)))), 
                  log=TRUE))+4 
  return(AIC)
}

ml_c_NbNc <- optim(c(1e-5, 1e-5), loglik_c_NbNc) 

ml_c_NbNc$value
ml_c_Nb$value
ml_c_Nc$value
 # NB+ nC is 26 lower than the Nc 

ml_novac_NbNc

loglik_c_AREA <- function(beta){
  betaCC <- beta[1]
  betaBC <- beta[2]
  
  AIC <-
    -2*sum(dbinom(GLM_cattle_data$case, GLM_cattle_data$S,
                  prob = (1-exp(-(betaCC*GLM_cattle_data$Exp/(GLM_cattle_data$AREA) +
                                    betaBC*GLM_cattle_data$ExpB/(GLM_cattle_data$AREA)))), 
                  log=TRUE))+4 
  return(AIC)
}

ml_c_area <- optim(c(1e-8, 1e-8), loglik_c_AREA) 
ml_c_area

ml_c_NbNc$value 
ml_c_Nb$value
ml_c_Nc$value 
ml_c_area$value 

mean(GLM_cattle_data$Nb/(GLM_cattle_data$Nb+ GLM_cattle_data$total_animals))


Model_structure <- data.frame(Model_structure  = c(rep("Nc", 6), 
                                                   rep("Nb", 6), 
                                                   rep("Nb+Nc", 6)))

Model_structure$data = rep(c(rep("cattle", 2), rep("novac", 2), rep("vac", 2)), 3)
Model_structure$parameter = rep(c("bcc","bbc", "bbub","bcub","bbvb","bcvb"), 3)
Model_structure$estimation = c(ml_c_Nc$par[1], ml_c_Nc$par[2], 
                        ml_novac_Nc$par[1], ml_novac_Nc$par[2], 
                        ml_vac_Nc$par[1], ml_vac_Nc$par[2], 
                        ml_c_Nb$par[1], ml_c_Nb$par[2], 
                        ml_novac_Nb$par[1], ml_novac_Nb$par[2],
                        ml_vac_Nb$par[1], ml_vac_Nb$par[2], 
                        ml_c_NbNc$par[1], ml_c_NbNc$par[2], 
                        ml_novac_NbNc$par[1], ml_novac_NbNc$par[2],
                        ml_vac_NbNc$par[1], ml_vac_NbNc$par[2])

Model_structure$AIC = c(ml_c_Nc$value, ml_c_Nc$value, 
                        ml_novac_Nc$value, ml_novac_Nc$value, 
                        ml_vac_Nc$value, ml_vac_Nc$value,
                        ml_c_Nb$value, ml_c_Nb$value, 
                        ml_novac_Nb$value, ml_novac_Nb$value, 
                        ml_vac_Nb$value, ml_vac_Nb$value, 
                        ml_c_NbNc$value, ml_c_NbNc$value, 
                        ml_novac_NbNc$value, ml_novac_NbNc$value, 
                        ml_vac_NbNc$value, ml_vac_NbNc$value)

write.csv(Model_structure, 
          file = "code/HPC/BC_estimation_GAM_predicted_prev/Model_selection.csv")
