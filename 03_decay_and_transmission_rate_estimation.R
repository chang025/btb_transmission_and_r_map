# Estimate the best fit decay rate 

decay <- c(seq(0.003, 0.0044, by= 0.0002), 0.0039, 0.0041)


decay_opt <- data.frame(decay, cattle_AIC = 0, vac_AIC = 0, novac_AIC = 0)
for (i in 1: length(decay)) {
  
  u = decay[i] 
  GLM_cattle_data <- read.csv(file =paste0("code/HPC/results/gam/GLM_cattle_decay",u,".csv"))
  
  GLM_cattle_data %>% 
    filter(ExpB != 0|Exp != 0|case <= 0)%>%
    filter(case < total_animals) -> GLM_cattle_data
  
  GLM_cattle_data %>%
    mutate(Nc = 
             ifelse((total_animals < 30), 30, total_animals)) -> GLM_cattle_data
  
  
  loglik_c_Nc <- function(beta){
    betaCC <- beta[1]
    betaBC <- beta[2]
    
    AIC <-
      -2*sum(dbinom(GLM_cattle_data$case, GLM_cattle_data$S,
                    prob = (1-exp(-(betaCC*GLM_cattle_data$Exp/GLM_cattle_data$Nc +
                                      betaBC*GLM_cattle_data$ExpB/GLM_cattle_data$Nc))), 
                    log=TRUE))+4 
    return(AIC)
  }
  
  ml_c_Nc <- optim(c(1e-5, 1e-6), loglik_c_Nc)
  decay_opt$cattle_AIC[i] <- ml_c_Nc$value
  
  Badger_Ind <- read.csv(file = paste0("code/HPC/results/gam/GLM_badger_decay_",u,".csv"))
 
  Badger_Ind %>% filter(vacc_status== 1) -> vac
  Badger_Ind %>% filter(vacc_status== 0) -> novac
  
  loglik_b2_novac_Nc <- function(beta){
    betaBB <- beta[1]
    betaCB <- beta[2]
    
    AIC <-
      -2*sum(dbinom(novac$result, 1,
                    prob = (1-exp(-(betaBB*novac$ExpB/novac$Nc2 +
                                      betaCB*novac$ExpC/novac$Nc2))), 
                    log=TRUE))+4 
    return(AIC)
  }
  
  ml_novac_Nc <- optim(c(1e-5, 1e-4), loglik_b2_novac_Nc)
  decay_opt$novac_AIC[i]<- ml_novac_Nc$value[1]
   
  vac %>% 
    mutate(Nb_Inma = ifelse(is.na(Nb_Inma), 1, Nb_Inma))-> vac 
  
  loglik_b2_vac_Nc <- function(beta){
    betaBB <- beta[1]
    betaCB <- beta[2]
    
    AIC <-
      -2*sum(dbinom(vac$result, 1,
                    prob = (1-exp(-(betaBB*vac$ExpB/vac$Nc2 +
                                      betaCB*vac$ExpC/vac$Nc2))), 
                    log=TRUE))+4 
    return(AIC)
  }
  
  ml_vac_Nc <- optim(c(1e-5, 1e-5), loglik_b2_vac_Nc)
  decay_opt$vac_AIC[i] <- ml_vac_Nc$value[1]
  
  print(i)
  
}

  decay_opt %>% mutate(AIC = cattle_AIC+ vac_AIC +novac_AIC) %>% 
    arrange(decay)-> decay_opt 
  decay_opt %>%  
  ggplot(aes(x = decay, y = AIC)) + 
    geom_smooth(se = FALSE, col = "black") +
    theme_bw()+ 
    geom_hline()
  
           
